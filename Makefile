CC = /usr/bin/gcc
CFLAGS = -pthread -g -Wall -Wextra -Werror -I.
LDFLAGS = -lpthread

all:file_creator.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o executable file_creator.c
clean:
	rm -f executable
run:
	./executable -C 6

