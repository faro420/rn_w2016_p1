/*
@author: Mir Farshid Baha
purpose: RNP-WS2016
version: v1.1
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>


#define BUFFLEN 5
#define PRODUCER 1
#define filename "/home/farshid/Downloads/C/rn_w2016_p1/names.txt"

//function prototypes
void* file_creator(void*args);
void* producer(void*args);

//declaration of synchronization elements
pthread_mutex_t mutex;
pthread_cond_t empty;
pthread_cond_t full;
pthread_barrier_t barrier;
int active;
int err;

//buffer related structs and variables
int put, take;
int fullEntries;

struct Line{
   char  line[80];
};



int main(int argc, char *argv[]){
   if( argc == 3 && argv[1][0]=='-' && argv[1][1]=='C'){
        int i;
        put=take=fullEntries=0;
        int number_of_consumers = atoi (argv[2]);
        err=pthread_mutex_init(&mutex, NULL);
        assert(!err);
        err=pthread_barrier_init(&barrier, NULL, number_of_consumers+PRODUCER);
        assert(!err);
        err=pthread_cond_init(&empty, NULL);
        assert(!err);
        err=pthread_cond_init(&full, NULL);
        assert(!err);
        active = 1;
        struct Line buffer[BUFFLEN];
        for(i=0;i<BUFFLEN;i++){
            memset(buffer[i].line,0,sizeof(buffer[0].line));           
        }         
        pthread_t threads[number_of_consumers+PRODUCER];
        for(i=0;i<number_of_consumers;i++){
            err=pthread_create(&threads[i],NULL,file_creator,(void*)buffer);
            assert(!err);
        }
        err=pthread_create(&threads[number_of_consumers],NULL,producer,(void*)buffer);
        assert(!err);
        while(active==1);
        for(i=number_of_consumers;i>=0;i--){
            err=pthread_join(threads[i],NULL);
            assert(!err);
        }
        err=pthread_mutex_destroy(&mutex);
        assert(!err);
        err=pthread_cond_destroy(&empty);
        assert(!err);
        err=pthread_cond_destroy(&full);
        assert(!err);   
        printf("\nALL THREADS JOINED SUCCESSFULY\n");
    }
    exit(EXIT_SUCCESS);
}



void* file_creator(void*args){ 
    pthread_barrier_wait(&barrier);
    while(active==1){
        pthread_mutex_lock(&mutex);
        struct Line* ptr = ((struct Line*)args);
        while (fullEntries == 0)   
            pthread_cond_wait(&full, &mutex);
        if(ptr[take].line[0]!='0'){
            FILE *fp = fopen( ptr[take].line, "w" );
            if (fp == NULL) exit(EXIT_FAILURE);
            fclose(fp);
            memset(ptr[take].line,0,sizeof(ptr[take].line));
            take = (take + 1) % BUFFLEN;
            fullEntries--; 
        }
        pthread_cond_broadcast(&empty);
        pthread_mutex_unlock( &mutex ); 
    } 
    pthread_mutex_unlock(&mutex);
    pthread_exit(NULL);
}

void* producer(void*args){
    pthread_barrier_wait(&barrier);
    FILE *fp = fopen( filename, "r" );
    if (fp == NULL) exit(EXIT_FAILURE);
    while(active==1){
        pthread_mutex_lock(&mutex);
        struct Line* ptr = ((struct Line*)args);       
        while (fullEntries == BUFFLEN) 
             pthread_cond_wait(&empty, &mutex); 
        char tmp[80];
        if(fgets(tmp, sizeof(tmp), fp )==NULL){
            active=0;
            fclose(fp); 
            pthread_cond_broadcast( &full ); 
            pthread_mutex_unlock( &mutex );
            pthread_exit(NULL);
        }else{   
            strcpy(ptr[put].line, tmp);    
            put = (put + 1) % BUFFLEN;   
            fullEntries++;             
            pthread_cond_broadcast(&full);             
            pthread_mutex_unlock(&mutex);
        }
    }
    pthread_cond_broadcast(&full);
    pthread_mutex_unlock( &mutex );        
    fclose(fp);
    pthread_exit(NULL);
}


